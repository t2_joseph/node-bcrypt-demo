const bcrypt = require('bcrypt');
const Benchmark = require('benchmark');

const hashPassword = (password, salt) => {
    bcrypt.hashSync(password, salt);
}

const checkPassword = (password, hash) => {
    bcrypt.compareSync(password, hash);
}

console.time("bcryptHash");
hashPassword("Password1", 13);
console.timeEnd("bcryptHash");

console.time("checkPassword");
checkPassword("Password1", "$2b$13$x97I.9seOmzrVZMnwTIMxOH2nVQ6sgY5p.uuyNVyVbFSOc6fdOkLe");
console.timeEnd("checkPassword");

const suite = new Benchmark.Suite;

suite.add("bcryptHashBenchMark", () => hashPassword("Password1", 13))
.on('cycle', (event) => {console.log(String(event.target))} )
.run({'async':true})
